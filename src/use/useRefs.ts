import type {Ref} from 'vue';
import {shallowReactive} from "vue";

type RefValueType<V> = V extends { use: { ref: () => Ref<infer Refer | null> } } ? Refer :
  V extends new (...args: any[]) => infer Refer ? Refer : V

export function useRefs<T extends { [k: string]: any }>(config: T): tRefs<T> {

  const refs = shallowReactive((() => {
    const obj = {} as any;
    for (let key in config) {
      obj[key] = undefined;
    }
    return obj;
  })());

  const onRef = (() => {
    const obj = {} as any;
    for (let key in config) {
      obj[key] = (refer: any) => {refs[key] = refer;};
    }
    return obj;
  })();

  return {
    refs,
    onRef,
  } as any;
}

export type tRefs<T extends { [k: string]: any }> = {
  refs: {
    [k in keyof T]: RefValueType<T[k]> | null | undefined
  },
  onRef: {
    [k in keyof T]: (val: any) => void
  },
}
