import {classnames} from "../utils/classnames";
import {computed} from "vue";

export type SingleClass = null | undefined | string | { [k: string]: boolean | null | undefined }
export type MultipleClass = SingleClass | SingleClass[]

export function useClasses<T extends () => MultipleClass>(fn: T) {
  return computed(() => classnames(fn()));
}
