import {reactive, watch} from 'vue';

function toNumber(val: null | string | number) {
  if (val == null) {
    return;
  }
  if (typeof val === "string") {
    return Number(val.replace('px', ''));
  }
  return val;
}


export function useNumber<Props, Keys extends keyof Props, Ret = Pick<Props, Keys>>(props: Props, keys: Keys[]): { numberState: { [k in keyof Ret]: Exclude<Ret[k], string> } } {

  const state = reactive((() => {
    const ret = {} as any;
    keys.forEach(key => {
      ret[key] = toNumber((props as any)[key]);
    });
    return ret;
  })());

  keys.forEach(key => {
    watch(() => props[key], (val: any) => state[key] = toNumber(val));
  });

  return {
    numberState: state as any,
  };

}

