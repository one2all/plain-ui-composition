import {ref, toRaw, watch} from 'vue';

export type UseModelConfig<T = any> = {
  autoEmit?: boolean | undefined,
  autoWatch?: boolean | undefined,
  onChange?: (newVal: T, oldVal: T) => void,
}

export type ModelType<T = any> = { value: T | null | undefined }

/**
 * 双向绑定组合函数
 * @author  韦胜健
 * @date    2020/10/19 9:21
 */
export function useModel<T>(
  getter: () => T,
  emitter: (val: T) => void,
  config?: UseModelConfig<T>
) {

  const state = ref(getter()) as { value: T };
  config = config || {};

  if (config.autoWatch !== false) {
    watch(getter, (val: T) => {
      if (toRaw(val) != toRaw(state.value)) {
        if (config!.onChange) {
          config!.onChange(val, state.value);
        }
        state.value = val;
      }
    });
  }

  return {
    get value() {
      return state.value;
    },
    set value(val: T) {
      state.value = val;
      if (config!.autoEmit !== false) {
        emitter((val));
      }
    }
  };
}
