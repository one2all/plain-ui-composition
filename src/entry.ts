export {designComponent,} from "./packages/designComponent";
export type {ComponentPropsType} from './packages/designComponent';
export {designPage} from "./packages/designPage";

export type {MultipleClass, SingleClass} from './use/useClasses';
export {useClasses} from "./use/useClasses";
export type {StyleProperties} from './use/useStyles';
export {useStyles} from "./use/useStyles";
export type {ModelType} from "./use/useModel";
export {useModel} from "./use/useModel";

export {useMounted} from "./use/useMounted";
export {useNumber} from "./use/useNumber";
export {useRefs} from "./use/useRefs";
export type {UseRefList} from './use/useRefList';
export {useRefList} from "./use/useRefList";
export {nextIndex} from "./utils/nextIndex";
export {useAsyncMethods} from './use/useAsyncMethods';
export {useCollect} from './use/useCollect';

export type {RenderNode, VueNode} from "./packages/designComponent.utils";
export type {ComponentEvent, EmitToProp, ObjectEmitOptions, SimpleFunction} from "./packages/emit";
export {createEventListener} from "./utils/createEventListener";
export type {iScopeSlotsOption} from "./packages/scopeSlots";
export type {InjectValue} from "./packages/inject";
export {error, log, warn} from "./utils/log";
import {getCurrentInstance, Teleport} from "vue";

export type {tSlotsType} from './packages/slot';
export type {tScopeSlotsType} from './packages/scopeSlots';
export type {tRefs} from './use/useRefs';

export {createHooks} from './utils/createHooks';
export {createSyncHooks} from './utils/createSyncHooks';
export type {PlainObject} from 'plain-utils/utils/event';
export * from './packages/inherit';
export * from 'vue';

export * from './utils/PlainEvent';
export {getComponentPrefix, setComponentPrefix, getComponentCls} from "./utils/componentPrefix";

export const getCurrentDesignInstance = () => getCurrentInstance()!;
export const Portal = Teleport;
export {getCurrentInstance};
export {createInject} from './utils/createInject';
export {createProvider} from './utils/createProvider';
export {createModule} from './utils/createModule';
export {mergeAttrs} from './utils/mergeAttrs';
export {createRenderHook} from './utils/createRenderHook';
export {cacheComputed} from './packages/cacheComputed';
