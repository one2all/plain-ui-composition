import {createLogger} from 'plain-utils/utils/createLogger'

export const log = createLogger('plain-ui-composition', 'log')
export const warn = createLogger('plain-ui-composition', 'warn')
export const error = createLogger('plain-ui-composition', 'error')