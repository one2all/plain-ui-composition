import {provide} from "vue";
import {createInject} from "./createInject";

/**
 * 创建自带有类型的provider
 * @author  韦胜健
 * @date    2022.6.29 11:25
 */
export function createProvider<T>(provideName: string) {
    return {
        provide: (val: T) => provide<T>(provideName, val),
        inject: createInject<T>(provideName),
    }
}
