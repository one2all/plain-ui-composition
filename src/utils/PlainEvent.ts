export type PlainMouseEvent = MouseEvent
export type PlainProgressEvent = ProgressEvent
export type PlainFocusEvent = FocusEvent
export type PlainKeyboardEvent = KeyboardEvent
export type PlainWheelEvent = WheelEvent

export default {}
