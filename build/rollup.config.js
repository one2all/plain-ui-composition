import nodeResolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import importResolver from "rollup-plugin-import-resolver";
import typescript from "@rollup/plugin-typescript";
import {terser} from "rollup-plugin-terser";

export default {
  input: './src/index.ts',
  external: [
    'vue',
    '@vue/runtime-core'
  ],
  plugins: [
    nodeResolve(),
    commonjs(),
    typescript({
      lib: [
        "esnext",
        "dom",
        "dom.iterable",
        "scripthost"
      ],
      target: "es5",
      strict: true,
      jsx: "preserve",
      importHelpers: true,
      moduleResolution: "node",
      skipLibCheck: true,
      esModuleInterop: true,
      noImplicitAny: true,
      importsNotUsedAsValues: "error",
      allowSyntheticDefaultImports: true,
      noEmit: true,
      sourceMap: true,

      include: [
        "src/**/*.ts",
        "src/**/*.tsx",
        "src/**/*.vue",
      ],
      "exclude": [
        "node_modules"
      ]
    }),
    importResolver({
      // a list of file extensions to check, default = ['.js']
      extensions: ['.js', '.ts'],
    }),
    terser()
  ],
  output: [
    {
      file: 'dist/plain-ui-composition.min.mjs',
      format: 'esm',
      name: 'PlainUiComposition',
      sourcemap: true,
      exports: 'named',
    },
    {
      file: 'dist/plain-ui-composition.min.js',
      format: 'esm',
      name: 'PlainUiComposition',
      sourcemap: true,
      exports: 'named',
    },
    {
      file: 'dist/plain-ui-composition.umd.min.js',
      format: 'umd',
      name: 'PlainUiComposition',
      sourcemap: true,
      exports: 'named',
      globals: {
        vue: 'Vue',
        '@vue/runtime-core': 'Vue'
      },
    }
  ],

};
