const path = require('path')
const resolve = filePath => path.resolve(__dirname, '../', filePath)
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const webpack = require("webpack")
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const outputDir = 'dist'

const config = {
    mode: 'production',
    entry: {
        index: resolve('src/index.ts'),
    },
    externals: {
        '@vue/runtime-core': {
            root: 'Vue',
            commonjs: '@vue/runtime-core',
            commonjs2: '@vue/runtime-core',
        },
        vue: {
            root: 'Vue',
            commonjs: 'vue',
            commonjs2: 'vue',
        },
    },
    output: {
        path: resolve(outputDir),
        filename: 'plain-ui-composition.min.js',
        libraryTarget: 'umd',
        // libraryExport: 'default',
        library: 'PlainUiComposition',
        globalObject: 'this'
    },
    resolve: {
        //别名
        alias: {
            "@": resolve('src'),// @指向src
            "~": resolve('node_modules')//~指向node_modules
        },
        //当你加载一个文件的时候,没有指定扩展名的时候，会自动寻找哪些扩展名
        extensions: [".ts", ".tsx", ".js", ".json"]
    },
    plugins: [
        new webpack.ProgressPlugin(),
        new ForkTsCheckerWebpackPlugin(),
        new webpack.optimize.LimitChunkCountPlugin({maxChunks: 1}),
        new BundleAnalyzerPlugin({analyzerMode: 'static'}),
    ],
    module: {
        rules: [
            {
                test: /\.(t|j)sx?$/,
                exclude: /node_modules(?!.*(plain-utils|plain-loading|plain-popper).*)/,
                use: [
                    {loader: 'babel-loader'},
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            happyPackMode: false,
                        },
                    },
                ],
            },
        ]
    },

}

module.exports = [
    config,
    /*{
        ...config,
        output: {
            path: resolve(outputDir),
            filename: 'plain-ui-composition.commonjs.min.js',
            libraryTarget: 'commonjs2',
        },
    }*/
]
