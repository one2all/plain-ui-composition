const path = require('path')
const resolve = filePath => path.resolve(__dirname, './', filePath)

module.exports = {
    pages: {
        index: {
            entry: resolve('story/main'),
            template: 'public/index.html',
            filename: 'index.html',
            title: 'plain-ui-composition',
            chunks: ['chunk-vendors', 'chunk-common', 'index'],
        },
    }
}
